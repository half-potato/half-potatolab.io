const lost = require('lost')
const pxtorem = require('postcss-pxtorem')

const url = 'https://lumen.netlify.com'

module.exports = {
  siteMetadata: {
    url,
    siteUrl: url,
    title: 'Alexander Mai\'s Homepage',
    subtitle:
      'PhD Student in DroneLab at UCSD',
    copyright: '© All rights reserved.',
    disqusShortname: '',
    menu: [
      {
        label: 'All',
        path: '/',
      },
      {
        label: 'Publications',
        path: '/categories/publications/',
      },
      {
        label: 'Projects',
        path: '/categories/projects/',
      },
    ],
    author: {
      name: 'Alexander Mai',
      email: 'amai@ucsd.edu',
      github: 'https://gitlab.com/half-potato',
      scholar: 'QG4shWwAAAAJ',
      linkedin: 'https://www.linkedin.com/in/alexandertmai/',
      telegram: '#',
      twitter: 'alexandertmai',
      rss: '#',
      vk: '#',
    },
  },
  plugins: [
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/src/pages`,
        name: 'pages',
      },
    },
    `gatsby-transformer-sharp`,
    {
      // resolve: 'gatsby-transformer-remark',
      resolve: 'gatsby-plugin-mdx',
      options: {
        extensions: ['.md', '.mdx'],
        // gfm: true,
        // footnotes: true,
        // excerpt_separator: `<!-- end -->`,
        gatsbyRemarkPlugins: [
          {
            resolve: 'gatsby-remark-images',
            options: {
              maxWidth: 960,
            },
          },
          {
            resolve: 'gatsby-remark-responsive-iframe',
            options: { wrapperStyle: 'margin-bottom: 1.0725rem; overflow: visible' },
          },
          'gatsby-remark-prismjs',
          `gatsby-remark-copy-linked-files`,
          {
            resolve: `gatsby-remark-smartypants`,
            options: {
              dashes: `oldschool`,
            },
          },
          `gatsby-remark-autolink-headers`,
          'gatsby-remark-katex',
          'gatsby-remark-graphviz',
        ],
      },
    },
    'gatsby-plugin-sharp',
    {
      resolve: 'gatsby-plugin-google-analytics',
      options: { trackingId: 'UA-73379983-2' },
    },
    {
      resolve: 'gatsby-plugin-google-fonts',
      options: {
        fonts: ['roboto:400,400i,500,700'],
      },
    },
    'gatsby-plugin-sitemap',
    'gatsby-plugin-offline',
    'gatsby-plugin-catch-links',
    'gatsby-plugin-react-helmet',
    {
      resolve: 'gatsby-plugin-sass',
      options: {
        implementation: require("sass"),
        postCssPlugins: [
          lost(),
          pxtorem({
            rootValue: 16,
            unitPrecision: 5,
            propList: [
              'font',
              'font-size',
              'line-height',
              'letter-spacing',
              'margin',
              'margin-top',
              'margin-left',
              'margin-bottom',
              'margin-right',
              'padding',
              'padding-top',
              'padding-left',
              'padding-bottom',
              'padding-right',
              'border-radius',
              'width',
              'max-width',
            ],
            selectorBlackList: [],
            replace: true,
            mediaQuery: false,
            minPixelValue: 0,
          }),
        ],
      },
    },
  ],
}
