ARG GATSBY_ACTIVE_ENV=develop
ARG GATSBY_ACTIVE_ENV
# FROM node:12-buster as build
#
# RUN yarn global add gatsby-cli
# ENV GATSBY_ACTIVE_ENV=$GATSBY_ACTIVE_ENV
#
# WORKDIR /app
# RUN yarn install
# RUN gatsby build
#
# FROM gatsbyjs/gatsby
# COPY --from=build /app/public /pub

FROM node:19-buster
RUN apt-get update && \
    apt-get install -y build-essential pkg-config libglib2.0-dev libjpeg-dev libpng-dev libtiff-dev libwebp-dev python3 python3-pip build-essential

# Install libvips 
RUN apt-get install -y libvips-dev

RUN yarn global add sass gatsby-cli 


WORKDIR /app
ADD . ./
RUN yarn
ENTRYPOINT ["gatsby", "develop", "--host", "0.0.0.0"]
# ENTRYPOINT ["bash"]
