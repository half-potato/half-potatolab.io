---
title: "EVER: Exact Volumetric Ellipsoid Rendering"
subtitle: for Real-time View Synthesis
date: "2024-08-12T18:31:00.000Z"
layout: post
draft: false
path: "/posts/ever/"
category: "Publications"
icon: "figure1.png"
authors: "
[Alexander Mai](https://half-potato.gitlab.io/),
[Peter Hedman](https://www.phogzone.com/),
[George Kopanas](https://grgkopanas.github.io/),
[Dor Verbin](https://dorverbin.github.io/), 
[David Futschik](https://scholar.google.com/citations?user=ozNFrecAAAAJ&hl=en),
[Qiangeng Xu](https://xharlie.github.io/),
[Falko Kuester](https://chei.ucsd.edu/team/fkuester/)
[Jonathan T. Barron](https://jonbarron.info/),
[Yinda Zhang](https://www.zhangyinda.com/),
"
tags:
  - "volume rendering"
  - "neural radiance fields"
  - "NeRF"
paper: "https://arxiv.org/abs/2410.01804"
code: "https://github.com/half-potato/ever_training"
journal: "Arxiv"
bibtex: "
@misc{mai2024everexactvolumetricellipsoid,
      title={EVER: Exact Volumetric Ellipsoid Rendering for Real-time View Synthesis}, 
      author={Alexander Mai and Peter Hedman and George Kopanas and Dor Verbin and David Futschik and Qiangeng Xu and Falko Kuester and Jonathan T. Barron and Yinda Zhang},
      year={2024},
      eprint={2410.01804},
      archivePrefix={arXiv},
      primaryClass={cs.CV},
      url={https://arxiv.org/abs/2410.01804}, 
}
"
description: "A method for real-time differentiable emission-only volume rendering"
---

import { ReactCompareSlider, ReactCompareSliderImage } from 'react-compare-slider';
import VideoComparison from '../../../components/VideoComparison/index.jsx';
import VidCompare from '../../../components/VidCompare';

An overview of the quality benefits of our EVER technique. Left: On the Zip-NeRF dataset, our model produces sharper and more accurate renderings than 3DGS and successor splatting-based techniques. Middle: Because our method correctly blends primitive colors according to the physics of volume rendering, it produces fewer ``foggy'' artifacts than splatting. Right: Our method correctly blends primitive colors, which is not possible using a splatting regardless of how primitives are sorted (globally or ray-wise).

# Abstract
We present Exact Volumetric Ellipsoid Rendering (EVER), a method for real-time differentiable emission-only volume rendering.
Unlike recent rasterization based approach by 3D Gaussian Splatting (3DGS), our primitive based representation allows for exact volume rendering, rather than alpha compositing 3D Gaussian billboards.
As such, unlike 3DGS our formulation does not suffer from popping artifacts and view dependent density, but still achieves frame rates of $\sim\!30$ FPS at 720p on an NVIDIA RTX4090. 
Because our approach is built upon ray tracing it supports rendering techniques such as defocus blur and camera distortion (e.g. such as from fisheye cameras), which are difficult to achieve by rasterization. 
We show that our method has higher performance and fewer blending issues than 3DGS and other subsequent works, especially on the challenging large-scale scenes from the Zip-NeRF dataset where it achieves SOTA results among real-time techniques.

<div class="center" style={{maxWidth: "65rem", marginLeft: "auto", marginRight: "auto"}}>
  <iframe width="100%" height="500rem" src="https://www.youtube.com/embed/dqLi2-v38LE?si=KtxK2qOiNoq_E-tE"
          title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
          allowfullscreen></iframe>
</div>

# Method

![](./figure2.png)
(a) Here we show a toy "flatland" scene containing two primitives (one red, one blue) with a camera orbiting them, viewed from above.
We render this orbit using three different techniques, where each camera position yields a one-dimensional "image" (a scanline) which are stacked vertically to produce these epipolar plane image (EPI) visualizations.
(b, c) The approximations made by approximate splatting-based techniques result in improper blending due to discontinuities, which are visible as horizontal lines across the EPI.
In contrast, (d) our method's exact rendering yields a smooth EPI, with bands of purple from color blending.

![](./figure3.png)
A visualization of our rendering procedure. Top: We cast a ray through a field of constant density ellipsoids and compute each ray-ellipsoid collision distance to get the endpoints of each step function. When the ray enters each primitive, the density along the ray increases. When it exits, the density drops back down a corresponding amount. Bottom: This lets us analytically integrate the volume rendering equation through the field.

<div style={{width: "100%"}}>
  <video width="100%" height="auto" autoPlay loop playsInline muted>
    <source src="/splinetracer/seq4.mp4" type="video/mp4"/>
  </video>
</div>

<div style={{width: "100%"}}>
  <video width="100%" height="auto" autoPlay loop playsInline muted>
    <source src="/splinetracer/epi2.mp4" type="video/mp4"/>
  </video>
</div>

# Qualitative Examples

<div style={{verticalAlign: "middle"}}>
  <div style={{width: "80%", marginLeft: 'auto', marginRight: 'auto'}}>
    <VidCompare name="berlin2_comp_3DGS" video="/splinetracer/berlin2_comb_3DGS.mp4"/>
  </div>
</div>

<div style={{verticalAlign: "middle"}}>
  <div style={{width: "80%", marginLeft: 'auto', marginRight: 'auto'}}>
    <VidCompare name="berlin4_comp_3DGS" video="/splinetracer/berlin4_comb_3DGS.mp4"/>
  </div>
</div>

<div style={{verticalAlign: "middle"}}>
  <div style={{width: "80%", marginLeft: 'auto', marginRight: 'auto'}}>
    <VidCompare name="london2_comp_3DGS" video="/splinetracer/london2_comb_3DGS.mp4"/>
  </div>
</div>

<div style={{verticalAlign: "middle"}}>
  <div style={{width: "80%", marginLeft: 'auto', marginRight: 'auto'}}>
    <VidCompare name="london_comp_STP" video="/splinetracer/london_comb_STP.mp4"/>
  </div>
</div>

<div style={{verticalAlign: "middle"}}>
  <div style={{width: "80%", marginLeft: 'auto', marginRight: 'auto'}}>
    <VidCompare name="nyc1_comp_3DGS" video="/splinetracer/nyc1_comb_3DGS.mp4"/>
  </div>
</div>

# EPI Videos

<div style={{verticalAlign: "middle"}}>
  <div style={{width: "80%", marginLeft: 'auto', marginRight: 'auto'}}>
    <VidCompare name="berlin5_comp_3DGS" video="/splinetracer/berlin5_comb_3DGS.mp4"/>
  </div>
</div>

<div style={{verticalAlign: "middle"}}>
  <div style={{width: "80%", marginLeft: 'auto', marginRight: 'auto'}}>
    <VidCompare name="train5_comp_STP" video="/splinetracer/train5_comb_STP.mp4"/>
  </div>
</div>

# Extra Datasets

<div style={{width: "100%"}}>
  <video width="100%" height="auto" autoPlay loop playsInline muted>
    <source src="/splinetracer/corsair2.mp4" type="video/mp4"/>
  </video>
</div>
US National Park Service 2023: Vought 4Fu Corsair Planewreck - Midway Island - Photogrammetry - Terrestrial . Collected by US National Park Service Submerged Resource Center . Distributed by Open Heritage 3D. https://doi.org/10.26301/8tt8-9f41

## Bibtex

