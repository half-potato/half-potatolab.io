---
title: Yonder Dynamics
subtitle: Robotics Club
date: "2021-08-01T16:51:00.000Z"
layout: post
draft: false
path: "/posts/yonder/"
category: "Projects"
icon: "yonder_rover.JPG"
code: "https://gitlab.com/Yonder-Dynamics"
website: "https://yonderdynamics.org"
tags:
  - "robotics"
  - "ROS"
description: "Software lead for Yonder Dynamics, a robotics student org."
---
[Yonder Dynamics](https://yonderdynamics.org/) is a fully student run robotics organization.  We compete in the [University Rover Challenge](http://urc.marssociety.org/) every year.  Everything on the robot is designed, manufactured, and coded by students. For the last 4 years, I have been software lead, helping lead efforts into developing state estimation, arm motion planning, and autonomous outdoor exploration. Here is the video we submitted to the URC the last year we were in person, demonstrating the capabilities of our rover.

<div className="center">
  <figure class="video_container">
    <iframe width="100%" height="400em" src="https://www.youtube.com/embed/766YikIs_aA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </figure>
</div>

![](./images/gazebo.jpg)
Our Gazebo Simulation

![](./images/exploration.png)
Exploration Algorithm Visualization

![](./images/artag.jpg)
ARTag Detection using YOLO

![Graph](./images/urc2018.JPG)
URC2018

![](./images/rover2020.jpg)
2020
![](./images/rover2018.JPG)
2018
![](./images/rover2017.JPG)
2017
