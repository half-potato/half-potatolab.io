---
title: Soft Expectation and Deep Maximization
subtitle: a method for training image feature detection
date: "2021-08-01T16:51:00.000Z"
layout: post
draft: false
path: "/posts/sedm/"
category: "Publications"
icon: "progression.png"
authors: "[Alexander Mai](https://half-potato.gitlab.io/) and [Allen Y. Yang](https://people.eecs.berkeley.edu/~yang/) and [Dominique E. Meyer](https://scholar.google.com/citations?user=OUDn40wAAAAJ&hl=en)"
tags:
  - "feature detection"
  - "deep learning"
paper: "https://arxiv.org/pdf/2104.10291.pdf"
code: "https://gitlab.com/confidence_painting/cpaint"
journal: "ICCV Workshop"
bibtex: "
@article{DBLP:journals/corr/abs-2104-10291,
  author    = {Alexander Mai and
               Allen Y. Yang and
               Dominique E. Meyer},
  title     = {Soft Expectation and Deep Maximization for Image Feature Detection},
  journal   = {CoRR},
  volume    = {abs/2104.10291},
  year      = {2021},
  url       = {https://arxiv.org/abs/2104.10291},
  archivePrefix = {arXiv},
  eprint    = {2104.10291},
  timestamp = {Mon, 26 Apr 2021 17:25:10 +0200},
  biburl    = {https://dblp.org/rec/journals/corr/abs-2104-10291.bib},
  bibsource = {dblp computer science bibliography, https://dblp.org}
}
"
description: "Using expectation maximization to optimize the repeatability of features."
---
