@article{DBLP:journals/corr/abs-2104-10291,
  author    = {Alexander Mai and
               Allen Y. Yang and
               Dominique E. Meyer},
  title     = {Soft Expectation and Deep Maximization for Image Feature Detection},
  journal   = {CoRR},
  volume    = {abs/2104.10291},
  year      = {2021},
  url       = {https://arxiv.org/abs/2104.10291},
  archivePrefix = {arXiv},
  eprint    = {2104.10291},
  timestamp = {Mon, 26 Apr 2021 17:25:10 +0200},
  biburl    = {https://dblp.org/rec/journals/corr/abs-2104-10291.bib},
  bibsource = {dblp computer science bibliography, https://dblp.org}
}
