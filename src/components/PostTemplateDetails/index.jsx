import React from 'react'
import { Link } from 'gatsby'
import moment from 'moment'
import Disqus from '../Disqus/Disqus'
import './style.scss'
import 'material-icons/iconfont/material-icons.css'
var showdown = require('showdown')
import { MDXProvider } from "gatsby-plugin-mdx";


import { useState } from "react";
import 'katex/dist/katex.min.css';

function is_defined(val) {
  return typeof val !== 'undefined' && val !== null
}

const get_text_file = async (filepath) => {
  const res = await fetch(filepath);

  // check for errors
  if (!res.ok) {
    throw res;
  }

  return res.text();
};
//
// Written by Dor Verbin, October 2021
// This is based on: http://thenewcode.com/364/Interactive-Before-and-After-Video-Comparison-in-HTML5-Canvas
// With additional modifications based on: https://jsfiddle.net/7sk5k4gp/13/

function playVids(vid, videoMerge) {
    // var videoMerge = document.getElementById(videoId + "Merge");
    // var vid = document.getElementById(videoId);

    var position = 0.5;
    var vidWidth = vid.videoWidth/2;
    var vidHeight = vid.videoHeight;

    var mergeContext = videoMerge.getContext("2d");
    var bcr = null;

    
    if (vid.readyState > 3) {
        vid.play();

        function trackLocation(e) {
            // Normalize to [0, 1]
            bcr = videoMerge.getBoundingClientRect();
            position = ((e.pageX - bcr.x) / bcr.width);
        }
        function trackLocationTouch(e) {
            // Normalize to [0, 1]
            bcr = videoMerge.getBoundingClientRect();
            position = ((e.touches[0].pageX - bcr.x) / bcr.width);
        }

        videoMerge.addEventListener("mousemove",  trackLocation, false); 
        videoMerge.addEventListener("touchstart", trackLocationTouch, false);
        videoMerge.addEventListener("touchmove",  trackLocationTouch, false);


        function drawLoop() {
            mergeContext.drawImage(vid, 0, 0, vidWidth, vidHeight, 0, 0, vidWidth, vidHeight);
            var colStart = (vidWidth * position).clamp(0.0, vidWidth);
            var colWidth = (vidWidth - (vidWidth * position)).clamp(0.0, vidWidth);
            mergeContext.drawImage(vid, colStart+vidWidth, 0, colWidth, vidHeight, colStart, 0, colWidth, vidHeight);
            requestAnimationFrame(drawLoop);

            
            var arrowLength = 0.09 * vidHeight;
            var arrowheadWidth = 0.025 * vidHeight;
            var arrowheadLength = 0.04 * vidHeight;
            var arrowPosY = vidHeight / 10;
            var arrowWidth = 0.007 * vidHeight;
            var currX = vidWidth * position;

            // Draw circle
            mergeContext.arc(currX, arrowPosY, arrowLength*0.7, 0, Math.PI * 2, false);
            mergeContext.fillStyle = "#FFD79340";
            mergeContext.fill()
            //mergeContext.strokeStyle = "#444444";
            //mergeContext.stroke()
            
            // Draw border
            mergeContext.beginPath();
            mergeContext.moveTo(vidWidth*position, 0);
            mergeContext.lineTo(vidWidth*position, vidHeight);
            mergeContext.closePath()
            mergeContext.strokeStyle = "#444444";
            mergeContext.lineWidth = 5;            
            mergeContext.stroke();

            // Draw arrow
            mergeContext.beginPath();
            mergeContext.moveTo(currX, arrowPosY - arrowWidth/2);
            
            // Move right until meeting arrow head
            mergeContext.lineTo(currX + arrowLength/2 - arrowheadLength/2, arrowPosY - arrowWidth/2);
            
            // Draw right arrow head
            mergeContext.lineTo(currX + arrowLength/2 - arrowheadLength/2, arrowPosY - arrowheadWidth/2);
            mergeContext.lineTo(currX + arrowLength/2, arrowPosY);
            mergeContext.lineTo(currX + arrowLength/2 - arrowheadLength/2, arrowPosY + arrowheadWidth/2);
            mergeContext.lineTo(currX + arrowLength/2 - arrowheadLength/2, arrowPosY + arrowWidth/2);

            // Go back to the left until meeting left arrow head
            mergeContext.lineTo(currX - arrowLength/2 + arrowheadLength/2, arrowPosY + arrowWidth/2);
            
            // Draw left arrow head
            mergeContext.lineTo(currX - arrowLength/2 + arrowheadLength/2, arrowPosY + arrowheadWidth/2);
            mergeContext.lineTo(currX - arrowLength/2, arrowPosY);
            mergeContext.lineTo(currX - arrowLength/2 + arrowheadLength/2, arrowPosY  - arrowheadWidth/2);
            mergeContext.lineTo(currX - arrowLength/2 + arrowheadLength/2, arrowPosY);
            
            mergeContext.lineTo(currX - arrowLength/2 + arrowheadLength/2, arrowPosY - arrowWidth/2);
            mergeContext.lineTo(currX, arrowPosY - arrowWidth/2);

            mergeContext.closePath();

            mergeContext.fillStyle = "#444444";
            mergeContext.fill();

            
            
        }
        requestAnimationFrame(drawLoop);
    } 
}

Number.prototype.clamp = function(min, max) {
  return Math.min(Math.max(this, min), max);
};
    
    
var resizeAndPlay = function (element)
{
    
    cv = document.getElementById(element.id+"Merge");
    console.log(element, cv);
    cv.current.width = element.current.videoWidth/2;
    cv.current.height = element.current.videoHeight;
    element.current.play();
    element.current.style.height = "0px";  // Hide video without stopping it
    playVids(element.current, cv.current);
}

// class PostTemplateDetails extends React.Component {
//   render() {
export default function PostTemplateDetails({data, children}) {
    const { somet, author } = data.site.siteMetadata
    const post = data.mdx
    const body = post.body;
    const { title, subtitle, icon, authors, paper, code, bibtex, website, journal } = post.frontmatter
    const tags = post.fields.tagSlugs

    var converter = new showdown.Converter()
    const authors_html = converter.makeHtml(authors);

    const homeBlock = (
      <div>
        <Link className="post-single__home-button" to="/">
          All Articles
        </Link>
      </div>
    )

    const tagsBlock = (
      <div className="post-single__tags">
        <ul className="post-single__tags-list">
          {tags &&
            tags.map((tag, i) => (
              <li className="post-single__tags-list-item" key={tag}>
                <Link to={tag} className="post-single__tags-list-item-link">
                  {post.frontmatter.tags[i]}
                </Link>
              </li>
            ))}
        </ul>
      </div>
    )

    const commentsBlock = (
      <div>
        <Disqus
          postNode={post}
          siteMetadata={data.site.siteMetadata}
        />
      </div>
    )

    return (
      <div>
        <div className="post-single">
            <div className="post-single__header">
              <h1 className="post-single__title">{title}</h1>
              <h1 className="post-single__subtitle">{subtitle}</h1>
              <hr style={{"boxSizing": 'content-box', borderTop: "1px solid rgba(0,0,0,.1)", height: "0", color: '#212529', width: '100%', marginTop: "1em", marginBottom: "0em"}}/>
              {is_defined(journal) &&
                <p className="post-single__journal">{journal}</p>
              }
              <p className="post-single__authors" dangerouslySetInnerHTML={{ __html: authors_html }} />
              <div className="post-single__center">
                <div className="post-single__links">
                  {is_defined(paper) &&
                      <Link className="post-single__link" to={paper}>
                        <svg style={{width:"48px", height:"48px"}} viewBox="0 0 24 24">
                          <path fill="currentColor" d="M16 0H8C6.9 0 6 .9 6 2V18C6 19.1 6.9 20 8 20H20C21.1 20 22 19.1 22 18V6L16 0M20 18H8V2H15V7H20V18M4 4V22H20V24H4C2.9 24 2 23.1 2 22V4H4M10 10V12H18V10H10M10 14V16H15V14H10Z" />
                        </svg><br/>
                        Paper
                      </Link>
                  }
                  {is_defined(code) &&
                      <Link className="post-single__link" to={code}>
                        <svg style={{width:"48px", height:"48px"}} viewBox="0 0 65 65">
                          <path fill="currentColor" d="M32 0a32.021 32.021 0 0 0-10.1 62.4c1.6.3 2.2-.7 2.2-1.5v-6c-8.9 1.9-10.8-3.8-10.8-3.8-1.5-3.7-3.6-4.7-3.6-4.7-2.9-2 .2-1.9.2-1.9 3.2.2 4.9 3.3 4.9 3.3 2.9 4.9 7.5 3.5 9.3 2.7a6.93 6.93 0 0 1 2-4.3c-7.1-.8-14.6-3.6-14.6-15.8a12.27 12.27 0 0 1 3.3-8.6 11.965 11.965 0 0 1 .3-8.5s2.7-.9 8.8 3.3a30.873 30.873 0 0 1 8-1.1 30.292 30.292 0 0 1 8 1.1c6.1-4.1 8.8-3.3 8.8-3.3a11.965 11.965 0 0 1 .3 8.5 12.1 12.1 0 0 1 3.3 8.6c0 12.3-7.5 15-14.6 15.8a7.746 7.746 0 0 1 2.2 5.9v8.8c0 .9.6 1.8 2.2 1.5A32.021 32.021 0 0 0 32 0z" />
                        </svg><br/>
                        code
                      </Link>
                  }
                </div>
              </div>
            </div>
          <div className="post-single__inner">
            <img
              src={icon.publicURL}
              className="post-single__image"
              width="90%"
              alt={icon.publicURL}
            />
            <div className="post-single__body" >
              {children}
            </div>


            {is_defined(bibtex) &&
              <div style={{width:"60%", marginLeft: 'auto', marginRight: 'auto'}}>
                <textarea class='bibtex'>
                  {bibtex}
                </textarea>
              </div>
            }

          </div>
          <div className="post-single__footer">
            <hr />
            <p className="post-single__footer-text">
            </p>
          </div>
        </div>
      </div>
    )
  }
// }
//
// export default PostTemplateDetails
