import React from 'react'
import './style.scss'
import '../../assets/fonts/fontello-f75d64fc/css/fontello.css'

class Links extends React.Component {
  render() {
    const links = this.props.data

    return (
      <div className="links">
        <ul className="links__list">
          <li className="links__list-item">
            <a
              href={`${links.github}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <i className="icon-git" />
            </a>
          </li>
          <li className="links__list-item">
            <a
              href={`${links.linkedin}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <i className="icon-linkedin" />
            </a>
          </li>
          <li className="links__list-item">
            <a
              href={`https://scholar.google.com/citations?user=${links.scholar}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <i className="icon-scholar" />
            </a>
          </li>
          <li className="links__list-item">
            <a href={`mailto:${links.email}`}>
              <i className="icon-mail" />
            </a>
          </li>
        </ul>
      </div>
    )
  }
}

export default Links
