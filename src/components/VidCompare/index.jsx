import React from 'react'
import { Link } from 'gatsby'
import moment from 'moment'
import './style.scss'
var showdown = require('showdown')

export default function VidCompare({name, video}) {
  return (
    <div class="video-compare-container" style={{width: "100%"}} dangerouslySetInnerHTML={{ __html: `
        <video class="video" id="${name}" loop playsinline autoPlay muted src="${video}" onplay="resizeAndPlay(this)"></video>
        <canvas height=0 class="videoMerge" id=${name+'Merge'}></canvas>
      ` }} />
  )
}
