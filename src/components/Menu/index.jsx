import React from 'react'
import { Link } from 'gatsby'
import './style.scss'
import resume from "../../../static/CV.pdf"

// class Menu extends React.Component {
//   render() {
export default function Menu({data}) {
    const menu = data

    const menuBlock = (
      <ul className="menu__list">
        {menu.map(item => (
          <li className="menu__list-item" key={item.path}>
            <Link
              to={item.path}
              className="menu__list-item-link"
              activeClassName="menu__list-item-link menu__list-item-link--active"
            >
              {item.label}
            </Link>
          </li>
        ))}
        <li className="menu__list-item" key={"/CV.pdf"}>
          <a href = "/CV.pdf" className="menu__list-item-link">
            Résumé
          </a>
        </li>
      </ul>
    )

    return <nav className="menu">{menuBlock}</nav>
  }
// }
//
// export default Menu
