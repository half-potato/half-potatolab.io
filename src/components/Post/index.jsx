import React from 'react'
import { Link } from 'gatsby'
import moment from 'moment'
import './style.scss'
var showdown = require('showdown')

function is_defined(val) {
  return typeof val !== 'undefined' && val !== null
}

class Post extends React.Component {
  render() {
    const {
      title,
      date,
      category,
      description,
      icon,
      code,
      paper,
      bibtex,
      journal,
      authors
    } = this.props.data.node.frontmatter
    const { slug, categorySlug } = this.props.data.node.fields
    const icon_path = is_defined(icon) ? icon.publicURL : null;

    var converter = new showdown.Converter()
    const authors_html = converter.makeHtml(authors);

    return (
      <div className="post">
        <div className="post__meta">
          {journal}
          <span className="post__meta-divider" />
          <time
            className="post__meta-time"
            dateTime={moment(date).format('MMMM D, YYYY')}
          >
            {moment(date).format('MMMM YYYY')}
          </time>
          <span className="post__meta-divider" />
          <span className="post__meta-category" key={categorySlug}>
            <Link to={categorySlug} className="post__meta-category-link">
              {category}
            </Link>
          </span>
        </div>
        <div className="post__card">
          <div className="post__preview-container">
            <a href={slug}>
              <img
                src={icon_path}
                className="post__preview-photo"
                width="100%"
                alt={icon_path}
              />
            </a>
          </div>
          <div className="post__text">
            <h2 className="post__title">
              <Link className="post__title-link" to={slug}>
                {title}
              </Link>
            </h2>
            <p className="post__authors" dangerouslySetInnerHTML={{ __html: authors_html }} />
            <p className="post__description">{description}</p>
            <div className="post__links">
              {is_defined(paper) &&
                  <Link className="post__link" to={paper}>
                    paper
                  </Link>
              }
              {is_defined(code) &&
                  <Link className="post__link" to={code}>
                    code
                  </Link>
              }
              <Link className="post__link" to={slug}>
                post
              </Link>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Post
