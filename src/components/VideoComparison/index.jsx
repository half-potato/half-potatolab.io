import React, { useRef, useState, useEffect } from 'react'
import './style.scss'
import ReactPlayer from 'react-player/lazy'

// Written by Dor Verbin, October 2021
// This is based on: http://thenewcode.com/364/Interactive-Before-and-After-Video-Comparison-in-HTML5-Canvas
// With additional modifications based on: https://jsfiddle.net/7sk5k4gp/13/

function playVids(vid, videoMerge) {
    // var videoMerge = document.getElementById(videoId + "Merge");
    // var vid = document.getElementById(videoId);

    var position = 0.5;
    var vidWidth = vid.videoWidth/2;
    var vidHeight = vid.videoHeight;

    var mergeContext = videoMerge.getContext("2d");
    var bcr = null;

    
    if (vid.readyState > 3) {
        vid.play();

        function trackLocation(e) {
            // Normalize to [0, 1]
            bcr = videoMerge.getBoundingClientRect();
            position = ((e.pageX - bcr.x) / bcr.width);
        }
        function trackLocationTouch(e) {
            // Normalize to [0, 1]
            bcr = videoMerge.getBoundingClientRect();
            position = ((e.touches[0].pageX - bcr.x) / bcr.width);
        }

        videoMerge.addEventListener("mousemove",  trackLocation, false); 
        videoMerge.addEventListener("touchstart", trackLocationTouch, false);
        videoMerge.addEventListener("touchmove",  trackLocationTouch, false);


        function drawLoop() {
            mergeContext.drawImage(vid, 0, 0, vidWidth, vidHeight, 0, 0, vidWidth, vidHeight);
            var colStart = (vidWidth * position).clamp(0.0, vidWidth);
            var colWidth = (vidWidth - (vidWidth * position)).clamp(0.0, vidWidth);
            mergeContext.drawImage(vid, colStart+vidWidth, 0, colWidth, vidHeight, colStart, 0, colWidth, vidHeight);
            requestAnimationFrame(drawLoop);

            
            var arrowLength = 0.09 * vidHeight;
            var arrowheadWidth = 0.025 * vidHeight;
            var arrowheadLength = 0.04 * vidHeight;
            var arrowPosY = vidHeight / 10;
            var arrowWidth = 0.007 * vidHeight;
            var currX = vidWidth * position;

            // Draw circle
            mergeContext.arc(currX, arrowPosY, arrowLength*0.7, 0, Math.PI * 2, false);
            mergeContext.fillStyle = "#FFD79340";
            mergeContext.fill()
            //mergeContext.strokeStyle = "#444444";
            //mergeContext.stroke()
            
            // Draw border
            mergeContext.beginPath();
            mergeContext.moveTo(vidWidth*position, 0);
            mergeContext.lineTo(vidWidth*position, vidHeight);
            mergeContext.closePath()
            mergeContext.strokeStyle = "#444444";
            mergeContext.lineWidth = 5;            
            mergeContext.stroke();

            // Draw arrow
            mergeContext.beginPath();
            mergeContext.moveTo(currX, arrowPosY - arrowWidth/2);
            
            // Move right until meeting arrow head
            mergeContext.lineTo(currX + arrowLength/2 - arrowheadLength/2, arrowPosY - arrowWidth/2);
            
            // Draw right arrow head
            mergeContext.lineTo(currX + arrowLength/2 - arrowheadLength/2, arrowPosY - arrowheadWidth/2);
            mergeContext.lineTo(currX + arrowLength/2, arrowPosY);
            mergeContext.lineTo(currX + arrowLength/2 - arrowheadLength/2, arrowPosY + arrowheadWidth/2);
            mergeContext.lineTo(currX + arrowLength/2 - arrowheadLength/2, arrowPosY + arrowWidth/2);

            // Go back to the left until meeting left arrow head
            mergeContext.lineTo(currX - arrowLength/2 + arrowheadLength/2, arrowPosY + arrowWidth/2);
            
            // Draw left arrow head
            mergeContext.lineTo(currX - arrowLength/2 + arrowheadLength/2, arrowPosY + arrowheadWidth/2);
            mergeContext.lineTo(currX - arrowLength/2, arrowPosY);
            mergeContext.lineTo(currX - arrowLength/2 + arrowheadLength/2, arrowPosY  - arrowheadWidth/2);
            mergeContext.lineTo(currX - arrowLength/2 + arrowheadLength/2, arrowPosY);
            
            mergeContext.lineTo(currX - arrowLength/2 + arrowheadLength/2, arrowPosY - arrowWidth/2);
            mergeContext.lineTo(currX, arrowPosY - arrowWidth/2);

            mergeContext.closePath();

            mergeContext.fillStyle = "#444444";
            mergeContext.fill();

            
            
        }
        requestAnimationFrame(drawLoop);
    } 
}

Number.prototype.clamp = function(min, max) {
  return Math.min(Math.max(this, min), max);
};
    
    
function resizeAndPlay(element, cv)
{
    
    // cv = document.getElementById(element.id+"Merge");
    console.log(element, cv);
    if (element.current === null) {
        return;
    }
    cv.current.width = element.current.videoWidth/2;
    cv.current.height = element.current.videoHeight;
    // element.current.play();
    // element.current.style.height = "0px";  // Hide video without stopping it
    playVids(element.current, cv.current);
}

// export default function VideoComparison({combined_video}) {
//     const vidRef = useRef(null);
//     const canvasRef = useRef(null);
//             // <video class="video" ref={vidRef} id="materials" loop playsinline controls autoPlay muted src={combined_video} onclick={resizeAndPlay(vidRef, canvasRef)}></video>
//     return (
//         <div class="video-compare-container" id="materialsDiv">
//             <ReactPlayer class="video" ref={vidRef} id="materials"
//                 playing={true}
//                 muted={true}
//                 loop={true}
//                 width="100%"
//                 height="100%"
//         controls={true}
//                 url={combined_video}
//                 onReady={resizeAndPlay(vidRef, canvasRef)}
//                 />
//             
//             <canvas height='0' ref={canvasRef} class="videoMerge" id="materialsMerge"></canvas>
//         </div>
//     )
// }

export default function VideoComparison({combined_video}) {
    const vidRef = useRef(null);
    const canvasRef = useRef(null);
	const [refVisible, setRefVisible] = useState(false);

	useEffect(() => {
	if (!refVisible) { return }
	}, refVisible);
	// <video class="video" ref={vidRef} id="materials" loop playsinline controls autoPlay muted src={combined_video} onclick={resizeAndPlay(vidRef, canvasRef)}></video>
            // <ReactPlayer class="video" ref={el => { vidRef.current = el; setRefVisible(!!el); }}
            //     id="materials"
            //     playing={true}
            //     muted={true}
            //     loop={true}
            //     width="100%"
            //     height="100%"
            //     controls={true}
            //     url={combined_video}
            //     onReady={resizeAndPlay(vidRef, canvasRef)}
            //     />
    return (
        <div class="video-compare-container" id="materialsDiv">
	        <video class="video" ref={el => { vidRef.current = el; setRefVisible(!!el); }}
	            id="materials" src={combined_video} onplay={resizeAndPlay(vidRef, canvasRef)}></video>
            <canvas height='0' ref={canvasRef} class="videoMerge" id="materialsMerge"></canvas>
        </div>
    )
}
