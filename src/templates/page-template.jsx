import React from 'react'
import Helmet from 'react-helmet'
import { useStaticQuery, graphql } from 'gatsby'
import Layout from '../components/Layout'
import PageTemplateDetails from '../components/PageTemplateDetails'

export default function PageTemplate({data, children}) {
    const { title, subtitle } = data.site.siteMetadata
    const page = data.mdx
    const { title: pageTitle, description: pageDescription } = page.frontmatter
    const description = pageDescription !== null ? pageDescription : subtitle

    return (
      <Layout>
        <div>
          <Helmet>
            <title>{`${pageTitle} - ${title}`}</title>
            <meta name="description" content={description} />
          </Helmet>
          <PageTemplateDetails data={data} children={children} />
        </div>
      </Layout>
    )
  }

export const pageQuery = graphql`
  query PageBySlug($slug: String!) {
    site {
      siteMetadata {
        title
        subtitle
        copyright
        menu {
          label
          path
        }
        author {
          name
          email
          telegram
          twitter
          github
          rss
          vk
        }
      }
    }
    mdx(fields: { slug: { eq: $slug } }) {
      id
      body
      internal {
        contentFilePath
      }
      frontmatter {
        title
        subtitle
        date
        description
        icon {
          publicURL
        }
        authors
        code
        bibtex
        paper
        website
        journal
      }
    }
  }
`
